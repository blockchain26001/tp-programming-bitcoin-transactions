# Use the official Debian image as the base image
FROM python:3.11

# Set the working directory inside the container
WORKDIR /app

# Copy all files from the host directory to the container directory
COPY . /app

RUN curl -sSL https://install.python-poetry.org | python3 -
RUN echo 'export PATH="/root/.local/bin:$PATH"' >> /root/.bashrc
RUN echo 'export BTCTOY_SECRET_PASSPHRASE="Athosianeheheheh78512"' >> /root/.bashrc


# Set the default command to run when the container starts
CMD ["bash"]
